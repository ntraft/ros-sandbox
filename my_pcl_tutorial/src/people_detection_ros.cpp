/*
 * Software License Agreement (BSD License)
 *
 * Point Cloud Library (PCL) - www.pointclouds.org
 * Copyright (c) 2013-, Open Perception, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 * * Neither the name of the copyright holder(s) nor the names of its
 * contributors may be used to endorse or promote products derived
 * from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * people_detection_ros.cpp
 * Created on: Aug 4, 2014
 * Author(s): Matteo Munaro, Neil Traft
 *
 * Example file for performing people detection on a Kinect live stream.
 * As a first step, the ground is manually initialized, then people detection is performed with the GroundBasedPeopleDetectionApp class,
 * which implements the people detection algorithm described here:
 * M. Munaro, F. Basso and E. Menegatti,
 * Tracking people within groups with RGB-D data,
 * In Proceedings of the International Conference on Intelligent Robots and Systems (IROS) 2012, Vilamoura (Portugal), 2012.
 */

#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <geometry_msgs/Pose.h>
#include <pcl_conversions/pcl_conversions.h> // provides serialization for PCLPointCloud2
#include <pcl/common/time.h>
#include <pcl/console/parse.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>    
#include <pcl/sample_consensus/sac_model_plane.h>
#include <pcl/people/ground_based_people_detection_app.h>

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloudT;

// Algorithm parameters //
std::string svm_filename = "data/hog-people-svm.yaml";
float min_confidence = -1.5;
float min_height = 1.3;
float max_height = 2.3;
float voxel_size = 0.06;
Eigen::Matrix3f rgb_intrinsics_matrix;
Eigen::VectorXf ground_coeffs;

// People Detection app //
ros::Publisher pub;
pcl::people::GroundBasedPeopleDetectionApp<PointT> people_detector;
bool first_point_cloud = true;
unsigned int ppl_count = 0;

int print_help()
{

	geometry_msgs::Pose ps;
	ps.position.x = 3;
	cout << "*******************************************************" << std::endl;
	cout << "Ground based people detection app options:" << std::endl;
	cout << "   --help    <show_this_help>" << std::endl;
	cout << "   --svm     <path_to_svm_file>" << std::endl;
	cout << "   --conf    <minimum_HOG_confidence (default = -1.5)>" << std::endl;
	cout << "   --min_h   <minimum_person_height (default = 1.3)>" << std::endl;
	cout << "   --max_h   <maximum_person_height (default = 2.3)>" << std::endl;
	cout << "*******************************************************" << std::endl;
	return 0;
}

struct callback_args{
	// structure used to pass arguments to the callback function
	PointCloudT::Ptr clicked_points_3d;
	pcl::visualization::PCLVisualizer::Ptr viewerPtr;
};

void pp_callback (const pcl::visualization::PointPickingEvent& event, void* args)
{
	struct callback_args* data = (struct callback_args *)args;
	if (event.getPointIndex () == -1)
		return;
	PointT current_point;
	event.getPoint(current_point.x, current_point.y, current_point.z);
	data->clicked_points_3d->points.push_back(current_point);
	// Draw clicked points in red:
	pcl::visualization::PointCloudColorHandlerCustom<PointT> red (data->clicked_points_3d, 255, 0, 0);
	data->viewerPtr->removePointCloud("clicked_points");
	data->viewerPtr->addPointCloud(data->clicked_points_3d, red, "clicked_points");
	data->viewerPtr->setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 10, "clicked_points");
	std::cout << current_point.x << " " << current_point.y << " " << current_point.z << std::endl;
}

void initialize_ground (PointCloudT::Ptr cloud)
{
	// Display pointcloud: (for ground initialization)
	pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("PCL Viewer"));
	pcl::visualization::PointCloudColorHandlerRGBField<PointT> rgb(cloud);
	viewer->addPointCloud<PointT> (cloud, rgb, "input_cloud");
	viewer->setCameraPosition(0,0,-2,0,-1,0,0);

	// Add point picking callback to viewer:
	struct callback_args cb_args;
	PointCloudT::Ptr clicked_points_3d (new PointCloudT);
	cb_args.clicked_points_3d = clicked_points_3d;
	cb_args.viewerPtr = viewer;
	viewer->registerPointPickingCallback (pp_callback, (void*)&cb_args);
	std::cout << "Shift+click on three floor points, then press 'Q'..." << std::endl;

	// Spin until 'Q' is pressed:
	viewer->spin();
	std::cout << "done." << std::endl;

	// Ground plane estimation:
	ground_coeffs.resize(4);
	std::vector<int> clicked_points_indices;
	for (unsigned int i = 0; i < clicked_points_3d->points.size(); i++)
		clicked_points_indices.push_back(i);
	pcl::SampleConsensusModelPlane<PointT> model_plane(clicked_points_3d);
	model_plane.computeModelCoefficients(clicked_points_indices, ground_coeffs);
	std::cout << "Ground plane: " << ground_coeffs(0) << " " << ground_coeffs(1) << " " << ground_coeffs(2) << " " << ground_coeffs(3) << std::endl;

	viewer->close();
}

void cloud_cb (const pcl::PCLPointCloud2ConstPtr& cloud_msg)
{
	PointCloudT::Ptr cloud(new PointCloudT);
	pcl::fromPCLPointCloud2 (*cloud_msg, *cloud);

	if (first_point_cloud)
	{
		first_point_cloud = false;
		initialize_ground(cloud);
		return;
	}

	// Perform people detection on the new cloud:
	std::vector<pcl::people::PersonCluster<PointT> > clusters;   // vector containing persons clusters
	people_detector.setInputCloud(cloud);
	people_detector.setGround(ground_coeffs);                    // set floor coefficients
	people_detector.compute(clusters);                           // perform people detection

	ground_coeffs = people_detector.getGround();                 // get updated floor coefficients

	// Draw cloud and people bounding boxes in the viewer:
	unsigned int k = 0;
	for(std::vector<pcl::people::PersonCluster<PointT> >::iterator it = clusters.begin(); it != clusters.end(); ++it)
	{
		if(it->getPersonConfidence() > min_confidence) // draw only people with confidence above a threshold
		{
			// Draw theoretical person bounding box in RViz:
			visualization_msgs::Marker m;
			m.header.frame_id = "/camera_depth_optical_frame";
			m.header.stamp = ros::Time::now();
			m.ns = "people";
			m.id = k;
			m.type = visualization_msgs::Marker::CUBE;
			m.action = visualization_msgs::Marker::ADD;

			Eigen::Vector3f center = it->getTCenter();
			m.pose.position.x = center[0];
			m.pose.position.y = center[1];
			m.pose.position.z = center[2];
			m.pose.orientation.x = 0.0;
			m.pose.orientation.y = 0.0;
			m.pose.orientation.z = 0.0;
			m.pose.orientation.w = 1.0;

			m.scale.x = 0.5;
			m.scale.y = it->getHeight();
			m.scale.z = 0.5;

			m.color.r = 0.0f;
			m.color.g = 1.0f;
			m.color.b = 0.0f;
			m.color.a = 1.0;

			m.lifetime = ros::Duration(1);
			pub.publish(m);

			k++;
		}
	}
	// Delete stale detections.
	for (int i = k; i < ppl_count; ++i) {
		visualization_msgs::Marker m;
		m.header.stamp = ros::Time::now();
		m.ns = "people";
		m.id = k;
		m.action = visualization_msgs::Marker::DELETE;
		pub.publish(m);
	}
	ppl_count = k;
	std::cout << k << " people found" << std::endl;
}

int main (int argc, char** argv)
{
	if(pcl::console::find_switch (argc, argv, "--help") || pcl::console::find_switch (argc, argv, "-h"))
		return print_help();

	rgb_intrinsics_matrix << 525, 0.0, 319.5, 0.0, 525, 239.5, 0.0, 0.0, 1.0; // Kinect RGB camera intrinsics

	// Read if some parameters are passed from command line:
	pcl::console::parse_argument (argc, argv, "--svm", svm_filename);
	pcl::console::parse_argument (argc, argv, "--conf", min_confidence);
	pcl::console::parse_argument (argc, argv, "--min_h", min_height);
	pcl::console::parse_argument (argc, argv, "--max_h", max_height);

	// Create classifier for people detection:
	pcl::people::PersonClassifier<pcl::RGB> person_classifier;
	person_classifier.loadSVMFromFile(svm_filename);   // load trained SVM

	// People detection app initialization:
	people_detector.setVoxelSize(voxel_size);					// set the voxel size
	people_detector.setIntrinsics(rgb_intrinsics_matrix);		// set RGB camera intrinsic parameters
	people_detector.setClassifier(person_classifier);			// set person classifier
	people_detector.setHeightLimits(min_height, max_height);	// set person classifier
//	people_detector.setSensorPortraitOrientation(true);			// set sensor orientation to vertical

	// Initialize ROS
	ros::init (argc, argv, "people");
	ros::NodeHandle nh;

	ros::Subscriber sub = nh.subscribe ("input", 1, cloud_cb);

	// We will publish bounding boxes for the people.
	pub = nh.advertise<visualization_msgs::Marker> ("people", 1);

	// Spin
	ros::spin ();
}

